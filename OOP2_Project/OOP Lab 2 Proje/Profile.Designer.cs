﻿
namespace OOP2_Project
{
    partial class Profile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Profile));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabProfil = new System.Windows.Forms.TabPage();
            this.btn_saveChanges = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_username = new System.Windows.Forms.Label();
            this.lbl_nameSurname = new System.Windows.Forms.Label();
            this.lbl_address = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.pnl_changePassword2 = new System.Windows.Forms.Panel();
            this.lbl_error_password = new System.Windows.Forms.Label();
            this.txtbox_newPassword = new System.Windows.Forms.TextBox();
            this.txtbox_oldPassword = new System.Windows.Forms.TextBox();
            this.pnl_changePassword1 = new System.Windows.Forms.Panel();
            this.lbl_newPassword = new System.Windows.Forms.Label();
            this.lbl_oldPassword = new System.Windows.Forms.Label();
            this.lbl_changePassword = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_error_email = new System.Windows.Forms.Label();
            this.txtbox_email = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_error_username = new System.Windows.Forms.Label();
            this.txtbox_username = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lbl_error_nameSurname = new System.Windows.Forms.Label();
            this.txtbox_nameSurn = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbl_error_address = new System.Windows.Forms.Label();
            this.txtbox_address = new System.Windows.Forms.TextBox();
            this.tab_orderExcgange = new System.Windows.Forms.TabPage();
            this.dataGridView_purchases = new System.Windows.Forms.DataGridView();
            this.btn_goShop = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabProfil.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.pnl_changePassword2.SuspendLayout();
            this.pnl_changePassword1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tab_orderExcgange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_purchases)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabProfil);
            this.tabControl1.Controls.Add(this.tab_orderExcgange);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(536, 521);
            this.tabControl1.TabIndex = 0;
            // 
            // tabProfil
            // 
            this.tabProfil.BackColor = System.Drawing.Color.Linen;
            this.tabProfil.Controls.Add(this.btn_saveChanges);
            this.tabProfil.Controls.Add(this.tableLayoutPanel1);
            this.tabProfil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabProfil.Location = new System.Drawing.Point(4, 29);
            this.tabProfil.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabProfil.Name = "tabProfil";
            this.tabProfil.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabProfil.Size = new System.Drawing.Size(528, 488);
            this.tabProfil.TabIndex = 0;
            this.tabProfil.Text = "                        Profile                        ";
            // 
            // btn_saveChanges
            // 
            this.btn_saveChanges.BackColor = System.Drawing.Color.White;
            this.btn_saveChanges.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_saveChanges.BackgroundImage")));
            this.btn_saveChanges.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_saveChanges.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_saveChanges.ForeColor = System.Drawing.Color.Sienna;
            this.btn_saveChanges.Location = new System.Drawing.Point(217, 430);
            this.btn_saveChanges.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_saveChanges.Name = "btn_saveChanges";
            this.btn_saveChanges.Size = new System.Drawing.Size(51, 51);
            this.btn_saveChanges.TabIndex = 1;
            this.btn_saveChanges.Text = "Save";
            this.btn_saveChanges.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_saveChanges.UseVisualStyleBackColor = false;
            this.btn_saveChanges.Click += new System.EventHandler(this.btn_saveChanges_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Controls.Add(this.lbl_username, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_nameSurname, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_address, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_email, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pnl_changePassword2, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.pnl_changePassword1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 3);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 4);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66852F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66852F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66852F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66852F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.32592F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(526, 424);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lbl_username
            // 
            this.lbl_username.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_username.AutoSize = true;
            this.lbl_username.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_username.Location = new System.Drawing.Point(65, 96);
            this.lbl_username.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_username.Name = "lbl_username";
            this.lbl_username.Size = new System.Drawing.Size(80, 18);
            this.lbl_username.TabIndex = 1;
            this.lbl_username.Text = "Username";
            // 
            // lbl_nameSurname
            // 
            this.lbl_nameSurname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_nameSurname.AutoSize = true;
            this.lbl_nameSurname.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_nameSurname.Location = new System.Drawing.Point(46, 166);
            this.lbl_nameSurname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_nameSurname.Name = "lbl_nameSurname";
            this.lbl_nameSurname.Size = new System.Drawing.Size(117, 18);
            this.lbl_nameSurname.TabIndex = 2;
            this.lbl_nameSurname.Text = "Name Surname";
            // 
            // lbl_address
            // 
            this.lbl_address.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_address.AutoSize = true;
            this.lbl_address.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_address.Location = new System.Drawing.Point(76, 236);
            this.lbl_address.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(58, 18);
            this.lbl_address.TabIndex = 3;
            this.lbl_address.Text = "Adress";
            // 
            // lbl_email
            // 
            this.lbl_email.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_email.Location = new System.Drawing.Point(78, 26);
            this.lbl_email.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(53, 18);
            this.lbl_email.TabIndex = 0;
            this.lbl_email.Text = "E-mail";
            // 
            // pnl_changePassword2
            // 
            this.pnl_changePassword2.Controls.Add(this.lbl_error_password);
            this.pnl_changePassword2.Controls.Add(this.txtbox_newPassword);
            this.pnl_changePassword2.Controls.Add(this.txtbox_oldPassword);
            this.pnl_changePassword2.Location = new System.Drawing.Point(212, 282);
            this.pnl_changePassword2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnl_changePassword2.Name = "pnl_changePassword2";
            this.pnl_changePassword2.Size = new System.Drawing.Size(311, 138);
            this.pnl_changePassword2.TabIndex = 9;
            // 
            // lbl_error_password
            // 
            this.lbl_error_password.AutoSize = true;
            this.lbl_error_password.Location = new System.Drawing.Point(45, 93);
            this.lbl_error_password.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_error_password.Name = "lbl_error_password";
            this.lbl_error_password.Size = new System.Drawing.Size(0, 18);
            this.lbl_error_password.TabIndex = 2;
            // 
            // txtbox_newPassword
            // 
            this.txtbox_newPassword.Location = new System.Drawing.Point(45, 66);
            this.txtbox_newPassword.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtbox_newPassword.Name = "txtbox_newPassword";
            this.txtbox_newPassword.Size = new System.Drawing.Size(222, 26);
            this.txtbox_newPassword.TabIndex = 1;
            // 
            // txtbox_oldPassword
            // 
            this.txtbox_oldPassword.Location = new System.Drawing.Point(45, 35);
            this.txtbox_oldPassword.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtbox_oldPassword.Name = "txtbox_oldPassword";
            this.txtbox_oldPassword.Size = new System.Drawing.Size(222, 26);
            this.txtbox_oldPassword.TabIndex = 0;
            // 
            // pnl_changePassword1
            // 
            this.pnl_changePassword1.Controls.Add(this.lbl_newPassword);
            this.pnl_changePassword1.Controls.Add(this.lbl_oldPassword);
            this.pnl_changePassword1.Controls.Add(this.lbl_changePassword);
            this.pnl_changePassword1.Location = new System.Drawing.Point(2, 282);
            this.pnl_changePassword1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnl_changePassword1.Name = "pnl_changePassword1";
            this.pnl_changePassword1.Size = new System.Drawing.Size(206, 138);
            this.pnl_changePassword1.TabIndex = 10;
            // 
            // lbl_newPassword
            // 
            this.lbl_newPassword.AutoSize = true;
            this.lbl_newPassword.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_newPassword.Location = new System.Drawing.Point(43, 69);
            this.lbl_newPassword.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_newPassword.Name = "lbl_newPassword";
            this.lbl_newPassword.Size = new System.Drawing.Size(113, 18);
            this.lbl_newPassword.TabIndex = 2;
            this.lbl_newPassword.Text = "New Password";
            // 
            // lbl_oldPassword
            // 
            this.lbl_oldPassword.AutoSize = true;
            this.lbl_oldPassword.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_oldPassword.Location = new System.Drawing.Point(46, 38);
            this.lbl_oldPassword.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_oldPassword.Name = "lbl_oldPassword";
            this.lbl_oldPassword.Size = new System.Drawing.Size(106, 18);
            this.lbl_oldPassword.TabIndex = 1;
            this.lbl_oldPassword.Text = "Old Password";
            // 
            // lbl_changePassword
            // 
            this.lbl_changePassword.AutoSize = true;
            this.lbl_changePassword.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_changePassword.Location = new System.Drawing.Point(31, 7);
            this.lbl_changePassword.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_changePassword.Name = "lbl_changePassword";
            this.lbl_changePassword.Size = new System.Drawing.Size(150, 19);
            this.lbl_changePassword.TabIndex = 0;
            this.lbl_changePassword.Text = "Change Password";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbl_error_email);
            this.panel1.Controls.Add(this.txtbox_email);
            this.panel1.Location = new System.Drawing.Point(212, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(311, 66);
            this.panel1.TabIndex = 11;
            // 
            // lbl_error_email
            // 
            this.lbl_error_email.AutoSize = true;
            this.lbl_error_email.Location = new System.Drawing.Point(42, 51);
            this.lbl_error_email.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_error_email.Name = "lbl_error_email";
            this.lbl_error_email.Size = new System.Drawing.Size(0, 18);
            this.lbl_error_email.TabIndex = 6;
            // 
            // txtbox_email
            // 
            this.txtbox_email.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtbox_email.Location = new System.Drawing.Point(44, 21);
            this.txtbox_email.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtbox_email.Name = "txtbox_email";
            this.txtbox_email.Size = new System.Drawing.Size(222, 26);
            this.txtbox_email.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lbl_error_username);
            this.panel2.Controls.Add(this.txtbox_username);
            this.panel2.Location = new System.Drawing.Point(212, 72);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(311, 66);
            this.panel2.TabIndex = 12;
            // 
            // lbl_error_username
            // 
            this.lbl_error_username.AutoSize = true;
            this.lbl_error_username.Location = new System.Drawing.Point(42, 50);
            this.lbl_error_username.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_error_username.Name = "lbl_error_username";
            this.lbl_error_username.Size = new System.Drawing.Size(0, 18);
            this.lbl_error_username.TabIndex = 7;
            // 
            // txtbox_username
            // 
            this.txtbox_username.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtbox_username.Location = new System.Drawing.Point(44, 21);
            this.txtbox_username.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtbox_username.Name = "txtbox_username";
            this.txtbox_username.Size = new System.Drawing.Size(222, 26);
            this.txtbox_username.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lbl_error_nameSurname);
            this.panel3.Controls.Add(this.txtbox_nameSurn);
            this.panel3.Location = new System.Drawing.Point(212, 142);
            this.panel3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(311, 66);
            this.panel3.TabIndex = 13;
            // 
            // lbl_error_nameSurname
            // 
            this.lbl_error_nameSurname.AutoSize = true;
            this.lbl_error_nameSurname.Location = new System.Drawing.Point(42, 50);
            this.lbl_error_nameSurname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_error_nameSurname.Name = "lbl_error_nameSurname";
            this.lbl_error_nameSurname.Size = new System.Drawing.Size(0, 18);
            this.lbl_error_nameSurname.TabIndex = 8;
            // 
            // txtbox_nameSurn
            // 
            this.txtbox_nameSurn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtbox_nameSurn.Location = new System.Drawing.Point(44, 21);
            this.txtbox_nameSurn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtbox_nameSurn.Name = "txtbox_nameSurn";
            this.txtbox_nameSurn.Size = new System.Drawing.Size(222, 26);
            this.txtbox_nameSurn.TabIndex = 7;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lbl_error_address);
            this.panel4.Controls.Add(this.txtbox_address);
            this.panel4.Location = new System.Drawing.Point(212, 212);
            this.panel4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(311, 66);
            this.panel4.TabIndex = 14;
            // 
            // lbl_error_address
            // 
            this.lbl_error_address.AutoSize = true;
            this.lbl_error_address.Location = new System.Drawing.Point(42, 50);
            this.lbl_error_address.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_error_address.Name = "lbl_error_address";
            this.lbl_error_address.Size = new System.Drawing.Size(0, 18);
            this.lbl_error_address.TabIndex = 9;
            // 
            // txtbox_address
            // 
            this.txtbox_address.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtbox_address.Location = new System.Drawing.Point(44, 21);
            this.txtbox_address.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtbox_address.Name = "txtbox_address";
            this.txtbox_address.Size = new System.Drawing.Size(222, 26);
            this.txtbox_address.TabIndex = 8;
            // 
            // tab_orderExcgange
            // 
            this.tab_orderExcgange.BackColor = System.Drawing.Color.Linen;
            this.tab_orderExcgange.Controls.Add(this.dataGridView_purchases);
            this.tab_orderExcgange.Location = new System.Drawing.Point(4, 29);
            this.tab_orderExcgange.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tab_orderExcgange.Name = "tab_orderExcgange";
            this.tab_orderExcgange.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tab_orderExcgange.Size = new System.Drawing.Size(528, 488);
            this.tab_orderExcgange.TabIndex = 1;
            this.tab_orderExcgange.Text = "                        Purchases                         ";
            // 
            // dataGridView_purchases
            // 
            this.dataGridView_purchases.AllowUserToAddRows = false;
            this.dataGridView_purchases.AllowUserToDeleteRows = false;
            this.dataGridView_purchases.AllowUserToResizeColumns = false;
            this.dataGridView_purchases.AllowUserToResizeRows = false;
            this.dataGridView_purchases.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_purchases.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_purchases.Location = new System.Drawing.Point(4, 100);
            this.dataGridView_purchases.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView_purchases.Name = "dataGridView_purchases";
            this.dataGridView_purchases.RowHeadersWidth = 51;
            this.dataGridView_purchases.RowTemplate.Height = 24;
            this.dataGridView_purchases.Size = new System.Drawing.Size(521, 283);
            this.dataGridView_purchases.TabIndex = 0;
            // 
            // btn_goShop
            // 
            this.btn_goShop.BackColor = System.Drawing.Color.White;
            this.btn_goShop.BackgroundImage = global::OOP2_Project.Properties.Resources.back;
            this.btn_goShop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_goShop.Location = new System.Drawing.Point(551, 438);
            this.btn_goShop.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_goShop.Name = "btn_goShop";
            this.btn_goShop.Size = new System.Drawing.Size(70, 65);
            this.btn_goShop.TabIndex = 1;
            this.btn_goShop.UseVisualStyleBackColor = false;
            this.btn_goShop.Click += new System.EventHandler(this.btn_goShop_Click);
            // 
            // Profile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Linen;
            this.ClientSize = new System.Drawing.Size(624, 517);
            this.Controls.Add(this.btn_goShop);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Profile";
            this.Text = "Profile";
            this.Load += new System.EventHandler(this.Profile_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabProfil.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.pnl_changePassword2.ResumeLayout(false);
            this.pnl_changePassword2.PerformLayout();
            this.pnl_changePassword1.ResumeLayout(false);
            this.pnl_changePassword1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tab_orderExcgange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_purchases)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabProfil;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabPage tab_orderExcgange;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.Label lbl_username;
        private System.Windows.Forms.Label lbl_nameSurname;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.TextBox txtbox_username;
        private System.Windows.Forms.TextBox txtbox_nameSurn;
        private System.Windows.Forms.TextBox txtbox_address;
        private System.Windows.Forms.Button btn_goShop;
        private System.Windows.Forms.Button btn_saveChanges;
        public System.Windows.Forms.TextBox txtbox_email;
        private System.Windows.Forms.Panel pnl_changePassword2;
        private System.Windows.Forms.TextBox txtbox_newPassword;
        private System.Windows.Forms.TextBox txtbox_oldPassword;
        private System.Windows.Forms.Panel pnl_changePassword1;
        private System.Windows.Forms.Label lbl_newPassword;
        private System.Windows.Forms.Label lbl_oldPassword;
        private System.Windows.Forms.Label lbl_changePassword;
        private System.Windows.Forms.Label lbl_error_password;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_error_email;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbl_error_username;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lbl_error_nameSurname;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbl_error_address;
        private System.Windows.Forms.DataGridView dataGridView_purchases;
    }
}