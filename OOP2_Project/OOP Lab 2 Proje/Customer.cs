﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP2_Project
{
    /*! \brief This class is for customers that already exist or new customers that will sign up.
    *  It has 6 private variables;
    *  @param customerID keeps the customer's unique id.
    *  @param nameSurname keeps the customer's name and surname.
    *  @param address keeps the customer's address.
    *  @param email keeps the customer's email address.
    *  @param username keeps the customer's username.
    *  @param password keeps the customer's password.
    */
    public class Customer
    {
        private int customerID;
        private string nameSurname;
        private string address;
        private string email;
        private string username;
        private string password;

        public int CustomerID { get => customerID; set => customerID = value; }
        public string NameSurname { get => nameSurname; set => nameSurname = value; }
        public string Address { get => address; set => address = value; }
        public string Email { get => email; set => email = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }

        public Customer()
        {

        }
       /*! \brief database den veri çekerken customerID otomatik 1 er artırılıyor o yüzden kodda işlemeye gerek kalmıyor.*/
        public Customer(int customerID, string nameSurname, string address, string email, string username, string password)
        {
            this.customerID = customerID;
            this.nameSurname = nameSurname;
            this.address = address;
            this.email = email;
            this.username = username;
            this.password = password;
        }
        /*! \brief kaydolurken id yok. id database e eklendikten sonra geliyor.*/
        public Customer(string nameSurname, string address, string email, string username, string password)
        {
            this.nameSurname = nameSurname;
            this.address = address;
            this.email = email;
            this.username = username;
            this.password = password;
        }

        public void saveCustomer(Customer updatedCustomer)
        {
            this.address = updatedCustomer.address;
            this.username = updatedCustomer.username;
            this.nameSurname = updatedCustomer.nameSurname;
            this.email = updatedCustomer.email;
            this.password = updatedCustomer.password;
            this.customerID = updatedCustomer.getID();

        }
        /*! \brief returns to the customer's id */
        public int getID() { return this.customerID; }


    }
}
