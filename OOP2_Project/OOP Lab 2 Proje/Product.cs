﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_Project
{
    /// <summary>
    /// This abstract class is ancestor of classes like books, CDs and magazines.
    /// </summary>
    abstract class Product
    {
        string name;
        string id;
        uint price;
        public abstract void printProperties();
    }
}
