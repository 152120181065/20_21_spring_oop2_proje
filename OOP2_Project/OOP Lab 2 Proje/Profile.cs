﻿using OOP2_Project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace OOP2_Project
{
    public partial class Profile : Form
    {
        Customer loggedCustomer = new Customer();
        Customer updatedTempCustomer = new Customer();
        List<Customer> customers = new List<Customer>();

        SqlConnection sqlConnection;
        SqlCommand sqlCommand;
        SqlDataAdapter sqlDataAdapter;
 
        public void setcustomers(List<Customer> aut_customers)
        {
            customers = aut_customers;
        }
        public void setCustomer(Customer autCostumer)
        {
            loggedCustomer = autCostumer;
        }
        public Profile()
        {
            InitializeComponent();

        }
        
        private void Profile_Load(object sender, EventArgs e)
        {
            txtbox_email.Text = loggedCustomer.Email;
            txtbox_username.Text = loggedCustomer.Username;
            txtbox_nameSurn.Text = loggedCustomer.NameSurname;
            txtbox_address.Text = loggedCustomer.Address;

            txtbox_oldPassword.PasswordChar = '*';
            txtbox_newPassword.PasswordChar = '*';

            sqlConnection = new SqlConnection("Data Source=SQL5097.site4now.net; Initial Catalog=db_a75582_oopproje; User ID=db_a75582_oopproje_admin; Password=7eQ2icRhm_au");
            sqlConnection.Open();

            sqlDataAdapter = new SqlDataAdapter("Select * From Purchases where customerID='" + loggedCustomer.getID() + "'", sqlConnection);
            DataSet ds = new DataSet();
            sqlDataAdapter.Fill(ds);

            dataGridView_purchases.DataSource = ds.Tables[0];
            sqlConnection.Close();

        }
        private void btn_goShop_Click(object sender, EventArgs e)
        {
            this.Hide();
            Shop.GetForm.setcustomers(customers);
            Shop.GetForm.setCustomer(loggedCustomer);
            Shop.GetForm.Show();
        }

        private void btn_saveChanges_Click(object sender, EventArgs e)
        {
            lbl_error_address.Text = "";
            lbl_error_email.Text = "";
            lbl_error_nameSurname.Text = "";
            lbl_error_username.Text = "";
            lbl_error_password.Text = "";

            updatedTempCustomer.CustomerID = loggedCustomer.getID();

            // E-MAİL CHANGE
            if (String.IsNullOrEmpty(txtbox_email.Text))
            {
                updatedTempCustomer.Email = loggedCustomer.Email;
                txtbox_email.Text = loggedCustomer.Email;
            }
            else
            {
                string tempMail = txtbox_email.Text;
                bool invalid = CompareLib.Compare.detect_hasAccount(customers, tempMail);
                if (invalid == false) // değiştirilmiş eposta sistemde kayıtlı değil. alınabilir
                {
                    updatedTempCustomer.Email = tempMail;

                    // database güncellemesi
                    sqlConnection = new SqlConnection("Data Source=SQL5097.site4now.net; Initial Catalog=db_a75582_oopproje; User ID=db_a75582_oopproje_admin; Password=7eQ2icRhm_au");
                    sqlConnection.Open();                  
                    string input = "Update Customers Set email=@email where customerID='" + loggedCustomer.getID() + "'";

                    sqlCommand = new SqlCommand(input, sqlConnection);
                    sqlCommand.Parameters.AddWithValue("@email", updatedTempCustomer.Email);
                    sqlCommand.ExecuteNonQuery();

                    sqlConnection.Close();
                }
                else if(invalid==true && tempMail == loggedCustomer.Email) // e-posta değiştirilmeden bırakılmışsa
                {
                    updatedTempCustomer.Email = loggedCustomer.Email;
                    txtbox_email.Text = loggedCustomer.Email;
                }
                else if (invalid == true && tempMail != loggedCustomer.Email) // eposta değiştirilmiş
                {
                    lbl_error_email.ForeColor = Color.Red;
                    lbl_error_email.Text = "This e-mail has already related with an another account.\n";

                    updatedTempCustomer.Email = loggedCustomer.Email;
                    txtbox_email.Text = loggedCustomer.Email;

                }
            }
            // USERNAME CHANGE
            if (String.IsNullOrEmpty(txtbox_username.Text))
            {
                updatedTempCustomer.Username = loggedCustomer.Username;
                txtbox_username.Text = loggedCustomer.Username;
            }
            else
            {
                string tempUsername = txtbox_username.Text;
                bool invalid = CompareLib.Compare.detect_such_a_username(customers, tempUsername);
                if (invalid == false) // değiştirilmiş username sistemde kayıtlı değil. alınabilir
                {
                    updatedTempCustomer.Username = tempUsername;

                    // database güncellemesi
                    sqlConnection = new SqlConnection("Data Source=SQL5097.site4now.net; Initial Catalog=db_a75582_oopproje; User ID=db_a75582_oopproje_admin; Password=7eQ2icRhm_au");
                    sqlConnection.Open();
                    string input = "Update Customers Set username=@username where customerID='" + loggedCustomer.getID() + "'";

                    sqlCommand = new SqlCommand(input, sqlConnection);
                    sqlCommand.Parameters.AddWithValue("@username", updatedTempCustomer.Username);
                    sqlCommand.ExecuteNonQuery();

                    sqlConnection.Close();
                }
                else if (invalid == true && tempUsername == loggedCustomer.Username) // userneme değiştirilmeden bırakılmışsa
                {
                    updatedTempCustomer.Username = loggedCustomer.Username;
                    txtbox_username.Text = loggedCustomer.Username;
                }
                else if (invalid == true && tempUsername != loggedCustomer.Username) // yeni girilen username başkası tarafından alınmış
                {
                    lbl_error_username.ForeColor = Color.Red;
                    lbl_error_username.Text = "This username has already taken.\n";

                    //updatedTempCustomer.Username = loggedCustomer.Username;
                    //txtbox_username.Text = loggedCustomer.Username;

                }
            }
            // NAME SURNAME

            if (String.IsNullOrEmpty(txtbox_nameSurn.Text))
            {
                updatedTempCustomer.NameSurname = loggedCustomer.NameSurname;
                txtbox_nameSurn.Text = loggedCustomer.NameSurname;
            }
            else
            {
                string tempNameSurn = txtbox_nameSurn.Text;
                
                if (tempNameSurn == loggedCustomer.NameSurname) //  name surname değiştirilmeden bırakılmışsa
                {
                    updatedTempCustomer.NameSurname = loggedCustomer.NameSurname;
                    txtbox_nameSurn.Text = loggedCustomer.NameSurname;
                }
                else if (tempNameSurn != loggedCustomer.NameSurname) //  name surname değiştirilmiş
                {
                    updatedTempCustomer.NameSurname = txtbox_nameSurn.Text  ;
                    loggedCustomer.NameSurname = updatedTempCustomer.NameSurname;

                    // veri tabanı burada güncellenmeli
                    // database güncellemesi
                    sqlConnection = new SqlConnection("Data Source=SQL5097.site4now.net; Initial Catalog=db_a75582_oopproje; User ID=db_a75582_oopproje_admin; Password=7eQ2icRhm_au");
                    sqlConnection.Open();

                    string input = "Update Customers Set nameSurname=@nameSurname where customerID='" + loggedCustomer.getID() + "'";

                    sqlCommand = new SqlCommand(input, sqlConnection);
                    sqlCommand.Parameters.AddWithValue("@nameSurname", updatedTempCustomer.NameSurname);
                    sqlCommand.ExecuteNonQuery();
                    sqlConnection.Close();

                }
            }
            // ADDRESS

            if (String.IsNullOrEmpty(txtbox_address.Text))
            {
                updatedTempCustomer.Address = loggedCustomer.Address;
                txtbox_address.Text = loggedCustomer.Address;
            }
            else
            {
                string tempAddress= txtbox_address.Text;

                if (tempAddress == loggedCustomer.Address) //  name surname değiştirilmeden bırakılmışsa
                {
                    updatedTempCustomer.Address = loggedCustomer.Address;
                    txtbox_address.Text = loggedCustomer.Address;
                }
                else if (tempAddress != loggedCustomer.Address) //  name surname değiştirilmiş
                {
                    updatedTempCustomer.Address = txtbox_address.Text;
                    loggedCustomer.Address  = updatedTempCustomer.Address;

                    // veri tabanı burada güncellenmeli
                    // database güncellemesi
                    sqlConnection = new SqlConnection("Data Source=SQL5097.site4now.net; Initial Catalog=db_a75582_oopproje; User ID=db_a75582_oopproje_admin; Password=7eQ2icRhm_au");
                    sqlConnection.Open();

                    string input = "Update Customers Set address=@address where customerID='" + loggedCustomer.getID() + "'";

                    sqlCommand = new SqlCommand(input, sqlConnection);
                    sqlCommand.Parameters.AddWithValue("@address", updatedTempCustomer.Address);
                    sqlCommand.ExecuteNonQuery();

                    sqlConnection.Close();
                }
            }
            // PASSWORD CHANGE

            if (String.IsNullOrEmpty(txtbox_oldPassword.Text) && String.IsNullOrEmpty(txtbox_newPassword.Text))
            {
                updatedTempCustomer.Password = loggedCustomer.Password;

            }
            else if((String.IsNullOrEmpty(txtbox_oldPassword.Text) && !String.IsNullOrEmpty(txtbox_newPassword.Text)) ||
                (!String.IsNullOrEmpty(txtbox_oldPassword.Text) && String.IsNullOrEmpty(txtbox_newPassword.Text)))
            {
                lbl_error_password.ForeColor = Color.Red;
                lbl_error_password.Text = "You must confirm your password";
                updatedTempCustomer.Password = loggedCustomer.Password;

            }
            else if(!String.IsNullOrEmpty(txtbox_newPassword.Text) && !String.IsNullOrEmpty(txtbox_oldPassword.Text))
            {
                if (txtbox_oldPassword.Text != loggedCustomer.Password)
                {
                    lbl_error_password.ForeColor = Color.Red;
                    lbl_error_password.Text = "You entered wrong your current password.";
                    updatedTempCustomer.Password = loggedCustomer.Password;

                }
                else
                {
                    updatedTempCustomer.Password = txtbox_newPassword.Text;

                    // database şifre güncelleme işlemi burada yapılır

                    sqlConnection = new SqlConnection("Data Source=SQL5097.site4now.net; Initial Catalog=db_a75582_oopproje; User ID=db_a75582_oopproje_admin; Password=7eQ2icRhm_au");
                    sqlConnection.Open();

                    string input = "Update Customers Set password=@password where customerID='" + loggedCustomer.getID() + "'";

                    sqlCommand = new SqlCommand(input, sqlConnection);
                    sqlCommand.Parameters.AddWithValue("@password", updatedTempCustomer.Password);
                    sqlCommand.ExecuteNonQuery();

                    sqlConnection.Close();
                }
            }

            loggedCustomer.saveCustomer(updatedTempCustomer);


            for (int i=0; i<customers.Count; i++)
            {
                if (customers[i].getID() == loggedCustomer.getID())
                {
                    customers[i] = loggedCustomer;
                    
                }
            }
        }
    }
}
