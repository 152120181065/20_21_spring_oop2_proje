﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace OOP2_Project
{
    public partial class ProductDescription : Form
    {
        String connectionstring = "Data Source=SQL5097.site4now.net;Initial Catalog=db_a75582_oopproje;User Id=db_a75582_oopproje_admin;Password=7eQ2icRhm_au";
        SqlConnection connection;
        SqlDataAdapter adapter;
        SqlCommand command;

        public Customer customer = new Customer(); //{ get { return this.customer; } set { } }
        string pr_name = "";
        string pr_isbn = "";
        string pr_price = "";
        string pr_type = "";
        string path = "";
        string pr_creator = "";
        string pr_page = "";
        string pr_issue = "";

        public ProductDescription()
        {
            InitializeComponent();
        }
        public void setPath(string item)  { path = item; }
        public void setName(string name)  { pr_name = name; }
        public void setType(string type) { pr_type = type; }
        public void setPrice(string price) { pr_price = price; }
        public void setISBN(string isbn) { pr_isbn = isbn; }
        public void setCreator(string creator) { pr_creator = creator; }
        public void setPageCount(string page) { pr_page = page; }
        public void setIssue(string issue) { pr_issue = issue; }
        private void ProductDescription_Load(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = path;
            lbName.Text = "Name: " + pr_name;
            if (pr_creator == "")
                lbCreator.Text = "Issue: " + pr_issue;
            else
                lbCreator.Text = "Creator: " + pr_creator;
            if (pr_isbn != "")
                lbISBN.Text = "ISBN: " + pr_isbn;
            else if (pr_type != "")
                lbISBN.Text = "Type: " + pr_type;
            if (pr_page != "")
                lbPage.Text = "Page Count: " + pr_page;
            lbPrice.Text = "Price: " + pr_price + " TL";
        }
        private void btnAddToCart_Click(object sender, EventArgs e)
        {
            if(txtAmountRuler() == true)
            {
                connection = new SqlConnection(connectionstring);
                connection.Open();

                string input = "Insert Into ShoppingCartS(customerID, productName, productType, quantity, unitPrice, totalPrice) values (@customerID, @productName, @productType, @quantity, @unitPrice, @totalPrice)";
                string selectID = "Select * from ShoppingCartS";

                adapter = new SqlDataAdapter(selectID, connection);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                bool flag = false;
                string dbname = "";
                string appname = "";
                int dbid = 0;
                int appid = 0;
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    dbname = dataTable.Rows[i]["productName"].ToString();
                    appname = pr_name;
                    dbid = Convert.ToInt32(dataTable.Rows[i]["customerID"]);
                    appid = customer.getID();

                    if (appid == dbid && appname == dbname)
                    {
                        string updateS = "Update ShoppingCartS set quantity = @quantity, totalPrice = @totalPrice where customerID ='" + customer.getID() + "' AND productName ='" + pr_name + "'";

                        SqlCommand update = new SqlCommand(updateS, connection);
                        if (Convert.ToInt32(txtAmount.Text.ToString()) < 1)
                        {
                            lblError.Text = "Amount cannot be smaller than 1.\nYour order will be processed with 1 amount.";
                            txtAmount.Text = "1";
                        }
                        int quanty = (Convert.ToInt32(dataTable.Rows[i]["quantity"].ToString()) + Convert.ToInt32(txtAmount.Text.ToString()));
                        int totalamount = (Convert.ToInt32(dataTable.Rows[i]["totalPrice"].ToString()) + (Convert.ToInt32(txtAmount.Text.ToString()) * Convert.ToInt32(pr_price)));
                       
                        update.Parameters.AddWithValue("@quantity", quanty);
                        update.Parameters.AddWithValue("@totalPrice", totalamount);
                        update.ExecuteNonQuery();
                        flag = true;
                    }
                }

                if (flag == false)
                {
                    command = new SqlCommand(input, connection);
                    command.Parameters.Add("@customerID", Convert.ToInt32(customer.getID()));
                    command.Parameters.Add("@productName", pr_name);
                    command.Parameters.Add("@productType", pr_type);
                    if (Convert.ToInt32(txtAmount.Text.ToString()) < 1)
                    {
                        lblError.Text = "Amount cannot be smaller than 1.\nYour order will be processed with 1 amount.";
                        txtAmount.Text = "1";
                    }
                    command.Parameters.Add("@quantity", Convert.ToInt32(txtAmount.Text.ToString()));
                    command.Parameters.Add("@unitPrice", Convert.ToInt32(pr_price));
                    command.Parameters.Add("@totalPrice", Convert.ToInt32(txtAmount.Text.ToString()) * Convert.ToUInt32(pr_price));
                    command.ExecuteNonQuery();
                }

                lb2.Text = "Product has been added.";

                connection.Close();
            }
        }
        private void btnGoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ProductDescription_FormClosed(object sender, FormClosedEventArgs e)
        {
            lbName.Text = "";
            lbCreator.Text = "";
            lbISBN.Text = "";
            lbPrice.Text = "";
            lbPage.Text = "";
        }

        private bool txtAmountRuler()
        {
            if (txtAmount.Text == "")
            {
                lblError.Text = "You must \nenter amount";
                return false;
            }
            else if (!int.TryParse(txtAmount.Text, out _))
            {
                lblError.Text = "Your amount must \nbe a number";
                return false;
            }
            else if (int.TryParse(txtAmount.Text, out _))
            {
                if (int.Parse(txtAmount.Text) > 100)
                {
                    lblError.Text = "Your amount must be \nless than 100";
                    return false;
                }
            }
            return true;
        }
    }
}
