﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_Project
{
    /*! \brief This class is a template for books.
    *  With this class and a list program can store books which customer bought/will buy.
    *  It is a descendant of abstract class Product.
    *  
    *  It has 6 values;
    *  @param name keeps name of the book.
    *  @param id keeps unique number of the book.
    *  @param price keeps price of the book.
    *  These three values are common with other inherited classes from product (CDs and Magazines).
    *  @param isbn keeps the book's isbn number.
    *  @param author keeps the book's author's name.
    *  @param page keeps the page count of the book.
    */
    class Book : Product
    {
        string name;
        uint id;
        uint price;
        long isbn;
        string author;
        uint page; 
        
        public string Name { get { return name; } set {} }
        public uint ID { get { return id; } set {} }
        public uint Price { get { return price; } set {} }
        public long Isbn { get { return isbn; } set {} }
        public string Author { get { return author; } set {} }
        public uint Page { get { return page; } set {} }
        
        public Book() { }
        public Book(string name, uint id, uint price, long isbn, string author, uint page) 
        {
            this.name = name;
            this.id = id;
            this.price = price;
            this.isbn = isbn;
            this.author = author;
            this.page = page;
        }
        
        public override void printProperties()
        {
            Console.WriteLine("--------------------" + Environment.NewLine + "Name: " + name + Environment.NewLine + "ID: " + id + Environment.NewLine + "Price: " + price + Environment.NewLine);
            Console.WriteLine("ISBN: " + isbn + Environment.NewLine + "Author: " + author + Environment.NewLine + "Page: " + page + Environment.NewLine + "--------------------" + Environment.NewLine);
        }
       
    }
}
