﻿
namespace OOP2_Project
{
    partial class Authorization
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl_sign = new System.Windows.Forms.TabControl();
            this.tabSignIn = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_login = new System.Windows.Forms.Button();
            this.lbl_si_error = new System.Windows.Forms.Label();
            this.txtB_si_password = new System.Windows.Forms.TextBox();
            this.txtB_si_username = new System.Windows.Forms.TextBox();
            this.lbl_si_password = new System.Windows.Forms.Label();
            this.lbl_si_username = new System.Windows.Forms.Label();
            this.tabSignUp = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.lbl_nameSurn = new System.Windows.Forms.Label();
            this.txtB_nameSurn = new System.Windows.Forms.TextBox();
            this.lbl_error = new System.Windows.Forms.Label();
            this.txtB_confirmPassword = new System.Windows.Forms.TextBox();
            this.txtB_password = new System.Windows.Forms.TextBox();
            this.txtB_address = new System.Windows.Forms.TextBox();
            this.txtB_username = new System.Windows.Forms.TextBox();
            this.txtB_email = new System.Windows.Forms.TextBox();
            this.lbl_confirmPassword = new System.Windows.Forms.Label();
            this.lbl_password = new System.Windows.Forms.Label();
            this.lbl_address = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.lbl_username = new System.Windows.Forms.Label();
            this.tabControl_sign.SuspendLayout();
            this.tabSignIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabSignUp.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl_sign
            // 
            this.tabControl_sign.Controls.Add(this.tabSignIn);
            this.tabControl_sign.Controls.Add(this.tabSignUp);
            this.tabControl_sign.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabControl_sign.HotTrack = true;
            this.tabControl_sign.ItemSize = new System.Drawing.Size(50, 30);
            this.tabControl_sign.Location = new System.Drawing.Point(-7, 0);
            this.tabControl_sign.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl_sign.Name = "tabControl_sign";
            this.tabControl_sign.Padding = new System.Drawing.Point(0, 0);
            this.tabControl_sign.SelectedIndex = 0;
            this.tabControl_sign.Size = new System.Drawing.Size(523, 610);
            this.tabControl_sign.TabIndex = 0;
            // 
            // tabSignIn
            // 
            this.tabSignIn.BackColor = System.Drawing.Color.Linen;
            this.tabSignIn.Controls.Add(this.pictureBox1);
            this.tabSignIn.Controls.Add(this.btn_login);
            this.tabSignIn.Controls.Add(this.lbl_si_error);
            this.tabSignIn.Controls.Add(this.txtB_si_password);
            this.tabSignIn.Controls.Add(this.txtB_si_username);
            this.tabSignIn.Controls.Add(this.lbl_si_password);
            this.tabSignIn.Controls.Add(this.lbl_si_username);
            this.tabSignIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabSignIn.Location = new System.Drawing.Point(4, 34);
            this.tabSignIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabSignIn.Name = "tabSignIn";
            this.tabSignIn.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabSignIn.Size = new System.Drawing.Size(515, 572);
            this.tabSignIn.TabIndex = 0;
            this.tabSignIn.Text = "                  Sign In                 ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::OOP2_Project.Properties.Resources.loginlogo;
            this.pictureBox1.Location = new System.Drawing.Point(148, 6);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(193, 180);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.Color.DimGray;
            this.btn_login.ForeColor = System.Drawing.Color.Salmon;
            this.btn_login.Location = new System.Drawing.Point(199, 426);
            this.btn_login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(93, 39);
            this.btn_login.TabIndex = 5;
            this.btn_login.Text = "Sign In";
            this.btn_login.UseVisualStyleBackColor = false;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // lbl_si_error
            // 
            this.lbl_si_error.AutoSize = true;
            this.lbl_si_error.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_si_error.Location = new System.Drawing.Point(68, 478);
            this.lbl_si_error.Name = "lbl_si_error";
            this.lbl_si_error.Size = new System.Drawing.Size(0, 20);
            this.lbl_si_error.TabIndex = 4;
            // 
            // txtB_si_password
            // 
            this.txtB_si_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtB_si_password.Location = new System.Drawing.Point(120, 353);
            this.txtB_si_password.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtB_si_password.Name = "txtB_si_password";
            this.txtB_si_password.Size = new System.Drawing.Size(249, 30);
            this.txtB_si_password.TabIndex = 3;
            // 
            // txtB_si_username
            // 
            this.txtB_si_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtB_si_username.Location = new System.Drawing.Point(120, 242);
            this.txtB_si_username.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtB_si_username.Name = "txtB_si_username";
            this.txtB_si_username.Size = new System.Drawing.Size(249, 30);
            this.txtB_si_username.TabIndex = 1;
            // 
            // lbl_si_password
            // 
            this.lbl_si_password.AutoSize = true;
            this.lbl_si_password.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_si_password.Location = new System.Drawing.Point(117, 324);
            this.lbl_si_password.Name = "lbl_si_password";
            this.lbl_si_password.Size = new System.Drawing.Size(117, 27);
            this.lbl_si_password.TabIndex = 2;
            this.lbl_si_password.Text = "Password";
            // 
            // lbl_si_username
            // 
            this.lbl_si_username.AutoSize = true;
            this.lbl_si_username.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_si_username.Location = new System.Drawing.Point(117, 210);
            this.lbl_si_username.Name = "lbl_si_username";
            this.lbl_si_username.Size = new System.Drawing.Size(122, 27);
            this.lbl_si_username.TabIndex = 0;
            this.lbl_si_username.Text = "Username";
            // 
            // tabSignUp
            // 
            this.tabSignUp.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.tabSignUp.BackColor = System.Drawing.Color.Linen;
            this.tabSignUp.Controls.Add(this.button1);
            this.tabSignUp.Controls.Add(this.lbl_nameSurn);
            this.tabSignUp.Controls.Add(this.txtB_nameSurn);
            this.tabSignUp.Controls.Add(this.lbl_error);
            this.tabSignUp.Controls.Add(this.txtB_confirmPassword);
            this.tabSignUp.Controls.Add(this.txtB_password);
            this.tabSignUp.Controls.Add(this.txtB_address);
            this.tabSignUp.Controls.Add(this.txtB_username);
            this.tabSignUp.Controls.Add(this.txtB_email);
            this.tabSignUp.Controls.Add(this.lbl_confirmPassword);
            this.tabSignUp.Controls.Add(this.lbl_password);
            this.tabSignUp.Controls.Add(this.lbl_address);
            this.tabSignUp.Controls.Add(this.lbl_email);
            this.tabSignUp.Controls.Add(this.lbl_username);
            this.tabSignUp.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabSignUp.Location = new System.Drawing.Point(4, 34);
            this.tabSignUp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabSignUp.Name = "tabSignUp";
            this.tabSignUp.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabSignUp.Size = new System.Drawing.Size(515, 572);
            this.tabSignUp.TabIndex = 1;
            this.tabSignUp.Text = "               Sign Up               ";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DimGray;
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button1.ForeColor = System.Drawing.Color.Salmon;
            this.button1.Location = new System.Drawing.Point(181, 462);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 44);
            this.button1.TabIndex = 14;
            this.button1.Text = "Sign Up";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbl_nameSurn
            // 
            this.lbl_nameSurn.AutoSize = true;
            this.lbl_nameSurn.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_nameSurn.Location = new System.Drawing.Point(95, 167);
            this.lbl_nameSurn.Name = "lbl_nameSurn";
            this.lbl_nameSurn.Size = new System.Drawing.Size(180, 27);
            this.lbl_nameSurn.TabIndex = 4;
            this.lbl_nameSurn.Text = "Name Surname";
            // 
            // txtB_nameSurn
            // 
            this.txtB_nameSurn.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtB_nameSurn.Location = new System.Drawing.Point(100, 199);
            this.txtB_nameSurn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtB_nameSurn.Name = "txtB_nameSurn";
            this.txtB_nameSurn.Size = new System.Drawing.Size(289, 35);
            this.txtB_nameSurn.TabIndex = 5;
            // 
            // lbl_error
            // 
            this.lbl_error.AutoSize = true;
            this.lbl_error.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_error.Location = new System.Drawing.Point(145, 512);
            this.lbl_error.Name = "lbl_error";
            this.lbl_error.Size = new System.Drawing.Size(0, 19);
            this.lbl_error.TabIndex = 12;
            // 
            // txtB_confirmPassword
            // 
            this.txtB_confirmPassword.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtB_confirmPassword.Location = new System.Drawing.Point(100, 406);
            this.txtB_confirmPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtB_confirmPassword.Name = "txtB_confirmPassword";
            this.txtB_confirmPassword.Size = new System.Drawing.Size(289, 35);
            this.txtB_confirmPassword.TabIndex = 11;
            // 
            // txtB_password
            // 
            this.txtB_password.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtB_password.Location = new System.Drawing.Point(100, 337);
            this.txtB_password.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtB_password.Name = "txtB_password";
            this.txtB_password.Size = new System.Drawing.Size(289, 35);
            this.txtB_password.TabIndex = 9;
            // 
            // txtB_address
            // 
            this.txtB_address.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtB_address.Location = new System.Drawing.Point(100, 268);
            this.txtB_address.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtB_address.Name = "txtB_address";
            this.txtB_address.Size = new System.Drawing.Size(289, 35);
            this.txtB_address.TabIndex = 7;
            // 
            // txtB_username
            // 
            this.txtB_username.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtB_username.Location = new System.Drawing.Point(100, 130);
            this.txtB_username.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtB_username.Name = "txtB_username";
            this.txtB_username.Size = new System.Drawing.Size(289, 35);
            this.txtB_username.TabIndex = 3;
            // 
            // txtB_email
            // 
            this.txtB_email.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtB_email.Location = new System.Drawing.Point(100, 62);
            this.txtB_email.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtB_email.Name = "txtB_email";
            this.txtB_email.Size = new System.Drawing.Size(289, 35);
            this.txtB_email.TabIndex = 1;
            // 
            // lbl_confirmPassword
            // 
            this.lbl_confirmPassword.AutoSize = true;
            this.lbl_confirmPassword.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_confirmPassword.Location = new System.Drawing.Point(95, 374);
            this.lbl_confirmPassword.Name = "lbl_confirmPassword";
            this.lbl_confirmPassword.Size = new System.Drawing.Size(208, 27);
            this.lbl_confirmPassword.TabIndex = 10;
            this.lbl_confirmPassword.Text = "Confirm Password";
            // 
            // lbl_password
            // 
            this.lbl_password.AutoSize = true;
            this.lbl_password.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_password.Location = new System.Drawing.Point(99, 305);
            this.lbl_password.Name = "lbl_password";
            this.lbl_password.Size = new System.Drawing.Size(117, 27);
            this.lbl_password.TabIndex = 8;
            this.lbl_password.Text = "Password";
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_address.Location = new System.Drawing.Point(95, 236);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(100, 27);
            this.lbl_address.TabIndex = 6;
            this.lbl_address.Text = "Address";
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_email.Location = new System.Drawing.Point(95, 30);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(80, 27);
            this.lbl_email.TabIndex = 0;
            this.lbl_email.Text = "E-mail";
            // 
            // lbl_username
            // 
            this.lbl_username.AutoSize = true;
            this.lbl_username.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_username.Location = new System.Drawing.Point(95, 98);
            this.lbl_username.Name = "lbl_username";
            this.lbl_username.Size = new System.Drawing.Size(122, 27);
            this.lbl_username.TabIndex = 2;
            this.lbl_username.Text = "Username";
            // 
            // Authorization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 609);
            this.Controls.Add(this.tabControl_sign);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Authorization";
            this.Text = "Authorization";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Authorization_FormClosed);
            this.tabControl_sign.ResumeLayout(false);
            this.tabSignIn.ResumeLayout(false);
            this.tabSignIn.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabSignUp.ResumeLayout(false);
            this.tabSignUp.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl_sign;
        private System.Windows.Forms.TabPage tabSignIn;
        private System.Windows.Forms.TabPage tabSignUp;
        private System.Windows.Forms.TextBox txtB_confirmPassword;
        private System.Windows.Forms.TextBox txtB_password;
        private System.Windows.Forms.TextBox txtB_address;
        private System.Windows.Forms.TextBox txtB_username;
        private System.Windows.Forms.TextBox txtB_email;
        private System.Windows.Forms.Label lbl_confirmPassword;
        private System.Windows.Forms.Label lbl_password;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.Label lbl_username;
        private System.Windows.Forms.Label lbl_error;
        private System.Windows.Forms.TextBox txtB_si_password;
        private System.Windows.Forms.TextBox txtB_si_username;
        private System.Windows.Forms.Label lbl_si_password;
        private System.Windows.Forms.Label lbl_si_username;
        private System.Windows.Forms.Label lbl_si_error;
        private System.Windows.Forms.TextBox txtB_nameSurn;
        private System.Windows.Forms.Label lbl_nameSurn;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
    }
}