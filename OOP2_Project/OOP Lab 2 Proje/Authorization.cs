﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace OOP2_Project
{
    public partial class Authorization : Form
    {
        SqlConnection sqlConnection;
        SqlCommand sqlCommand;
        SqlDataAdapter sqlDataAdapter;
        Customer loggedCustomer = new Customer();

        List<Customer> customers = new List<Customer>();

        private static Authorization inst;

        public static Authorization GetForm
        {
            get
            {
                if (inst == null || inst.IsDisposed)
                    inst = new Authorization();
                return inst;

            }
        }

        public Authorization()
        {
            InitializeComponent();
            txtB_si_password.PasswordChar = '*';
            txtB_password.PasswordChar = '*';
            txtB_confirmPassword.PasswordChar = '*';


            sqlConnection = new SqlConnection("Data Source=SQL5097.site4now.net; Initial Catalog=db_a75582_oopproje; User ID=db_a75582_oopproje_admin; Password=7eQ2icRhm_au");
            sqlConnection.Open();

            sqlDataAdapter = new SqlDataAdapter("select * from Customers", sqlConnection);

            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Customer customer = new Customer(int.Parse(dataTable.Rows[i]["customerID"].ToString()),
                                                dataTable.Rows[i]["nameSurname"].ToString(),
                                                dataTable.Rows[i]["address"].ToString(),
                                                dataTable.Rows[i]["email"].ToString(),
                                                dataTable.Rows[i]["username"].ToString(),
                                                dataTable.Rows[i]["password"].ToString());
                customers.Add(customer);
            }
            sqlConnection.Close();
        }
  
        private void btn_login_Click(object sender, EventArgs e)
        {
            // şifre ve kullanıcı adı text box larının boş olması durumu 
            if (String.IsNullOrEmpty(txtB_si_username.Text))
            {
                if (String.IsNullOrEmpty(txtB_si_username.Text))
                {
                    lbl_si_error.Text = "You must enter your username and password.";
                    lbl_si_error.ForeColor = Color.Red;
                    return;
                }
                else
                {
                    lbl_si_error.Text = "You must enter a username.";
                    lbl_si_error.ForeColor = Color.Red;
                    return;
                }

            }
            else
            {
                if (String.IsNullOrEmpty(txtB_si_password.Text))
                {
                    lbl_si_error.Text = "You must enter a password.";
                    lbl_si_error.ForeColor = Color.Red;
                    return;
                }

            }

            bool hasAccount = CompareLib.Compare.detect_such_a_username(customers, txtB_si_username.Text);
            bool permission = CompareLib.Compare.detect_Permission(customers, txtB_si_username.Text, txtB_si_password.Text);

            if (hasAccount == true) // hesap var
            {
                if (permission == false) // yanlış şifre
                {
                    lbl_si_error.Text = "Wrong Password! Try Again. ";
                    lbl_si_error.ForeColor = Color.Red;
                }
                else if (permission == true)
                {
                    lbl_si_error.Text = "Login Successfully.";

                    // giriş yapmış kullanıcıyı belirleme
                    for (int i = 0; i < customers.Count; i++)
                    {
                        if (txtB_si_username.Text == customers[i].Username)
                        {
                            loggedCustomer = customers[i];
                            break;
                        }
                    }

                    // shop sayfsını açma işlemleri
                    this.Hide();
                    
                    Shop.GetForm.setCustomer(loggedCustomer);
                    Shop.GetForm.setcustomers(customers);
                    Shop.GetForm.Show();

                }
            }
            else // böyle bir hesap yok
            {
                lbl_si_error.Text = "No Such User Here! Please, Sign Up First. ";
                lbl_si_error.ForeColor = Color.Red;
            }

        }
        private bool detect_hasAccount(List<Customer> customers, string TxtInput_email)
        {
            for (int i = 0; i < customers.Count; i++)
            {
                if (TxtInput_email == customers[i].Email) return true;
            }
            return false;
        }

        private void Authorization_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
                Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool hasAccount = detect_hasAccount(customers, txtB_email.Text);

            // bu email kayıtlı mı?
            if (hasAccount == true)
            {
                lbl_error.ForeColor = Color.Red;
                lbl_error.Text = "This e-mail is already registered. \nIf it is yours, please sign in.";
                return;

            }

            // bu username alınmış mı?
            bool invalidUsername = CompareLib.Compare.detect_such_a_username(customers, txtB_username.Text);
            if (invalidUsername == true)
            {
                lbl_error.ForeColor = Color.Red;
                lbl_error.Text = "This username is already taken.\nChoose another one.";
                return;
            }

            // name-surname, address password başkalarınınki ile aynı olabilir o yüzden check etmeye gerek yok
            // sadece boş mu değil mi diye bak
            // daha sonra adres için şehir, ilçe seçimi yaptıran bi arayüz koyabilirim
            // mahalle sokak ve binayı elle girsin string te toplayayım hepsini.

            if (String.IsNullOrEmpty(txtB_email.Text) ||
                String.IsNullOrEmpty(txtB_username.Text) ||
                String.IsNullOrEmpty(txtB_nameSurn.Text) ||
                String.IsNullOrEmpty(txtB_address.Text))
            {
                lbl_error.ForeColor = Color.Red;
                lbl_error.Text = "All fields must be required for registration.";
            }


            // şifre doğrulaması kontrolü
            if ((String.IsNullOrEmpty(txtB_password.Text) || String.IsNullOrEmpty(txtB_confirmPassword.Text))
                & (!String.IsNullOrEmpty(txtB_email.Text) &
                !String.IsNullOrEmpty(txtB_username.Text) &
                !String.IsNullOrEmpty(txtB_nameSurn.Text) &
                !String.IsNullOrEmpty(txtB_address.Text)))
            {

                lbl_error.Text = "You must confirm your password.";
                lbl_error.ForeColor = Color.Red;
                return;

            }

            if (hasAccount == false && invalidUsername == false) // kayıt gerçekleşir.
            {
                Customer newCustomer = new Customer(txtB_nameSurn.Text, txtB_address.Text, txtB_email.Text, txtB_username.Text, txtB_password.Text);
                //customers.Add(newCustomer);


                // yeni kullanıcıyı  database e insert ediyorum.
                sqlConnection = new SqlConnection("Data Source=SQL5097.site4now.net; Initial Catalog=db_a75582_oopproje; User ID=db_a75582_oopproje_admin; Password=7eQ2icRhm_au");
                sqlConnection.Open();


                string sorgu = "Insert Into Customers(nameSurname,address,email,username,password) values(@nameSurname,@address,@email,@username,@password)";

                sqlCommand = new SqlCommand(sorgu, sqlConnection);

                sqlCommand.Parameters.AddWithValue("@nameSurname", txtB_nameSurn.Text);
                sqlCommand.Parameters.AddWithValue("@address", txtB_address.Text);
                sqlCommand.Parameters.AddWithValue("@email", txtB_email.Text);
                sqlCommand.Parameters.AddWithValue("@username", txtB_username.Text);
                sqlCommand.Parameters.AddWithValue("@password", txtB_password.Text);

                sqlCommand.ExecuteNonQuery();

                sqlConnection.Close();

                // yeni kullanıcı eklendikten sonra bu form oluşturulurken veritabanı verilerini aktardığım customers dizisini tekrar güncellemem gerekiyor.
                // aynı işlemi iki kere yaptığım için fonksiyon yazabilirim.
                sqlConnection = new SqlConnection("Data Source=SQL5097.site4now.net; Initial Catalog=db_a75582_oopproje; User ID=db_a75582_oopproje_admin; Password=7eQ2icRhm_au");
                sqlConnection.Open();

                sqlDataAdapter = new SqlDataAdapter("select * from Customers", sqlConnection);

                DataTable dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    Customer customer = new Customer(int.Parse(dataTable.Rows[i]["customerID"].ToString()),
                                                    dataTable.Rows[i]["nameSurname"].ToString(),
                                                    dataTable.Rows[i]["address"].ToString(),
                                                    dataTable.Rows[i]["email"].ToString(),
                                                    dataTable.Rows[i]["username"].ToString(),
                                                    dataTable.Rows[i]["password"].ToString());
                    customers.Add(customer);
                }

                sqlConnection.Close();
                lbl_error.ForeColor = System.Drawing.Color.Green;
                lbl_error.Text = "Succesfully signed up.\n Now you can sign in.";
                
                lbl_address.Text = "";
                lbl_confirmPassword.Text = "";
                lbl_email.Text = "";
                lbl_nameSurn.Text = "";
                lbl_password.Text = "";
                lbl_username.Text = "";

            }
        }
    }
}
