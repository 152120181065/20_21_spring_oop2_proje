﻿using OOP2_Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareLib
{
     /// <summary>
     /// This class compares customer accounts with one another to see if there already exists an account with that email.
     /// And other processes like that.
     /// It helps with keeping the customer accounts unique.
     /// </summary>
    public static class Compare
    {
        ///böyle bir maille ilişkilendirilmiş kullanıcı hesabı olup olmadığını kontrol eder
        public static bool detect_hasAccount(List<Customer> customers, string TxtInput_email)
        {
            for (int i = 0; i < customers.Count; i++)
            {
                if (TxtInput_email == customers[i].Email) return true;
            }
            return false;
        }

        /// kullanıcı adı veritabanında bulunuyor mu diye baktığımız fonksiyonumuz:
        public static bool detect_such_a_username(List<Customer> customers, string TxtInput_username)
        {
            for (int i = 0; i < customers.Count; i++)
            {
                if (TxtInput_username == customers[i].Username) return true;
            }
            return false;
        }

        ///login esnasında şifre kontrolü
        public static bool detect_Permission(List<Customer> customers, string TxtInput_username, string TxtInput_password)
        {
            for (int i = 0; i < customers.Count; i++)
            {
                if (TxtInput_username == customers[i].Username)
                {
                    if (TxtInput_password == customers[i].Password)
                    {
                        return true;
                    }
                    /// şifrsini yanlış giren dikkatsiz bir kullanıcı. kapıdan göndermek için 0 return ediyoruz.
                    return false; 
                }
            }
            /// böyle bir kullanıcı yok ki şifresi doğru olsun
            return false;  
        }

        ///girilen iki metin kusundaki stringlerin aynı olup olmadığını kontrol eder.
        public static bool confirmPassword(string _old, string _new)
        {
            if (_old == _new) return true;
            else return false;
        }
    }
}
