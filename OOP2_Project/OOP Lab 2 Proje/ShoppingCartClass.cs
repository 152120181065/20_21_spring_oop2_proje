﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace OOP2_Project
{
    /// <summary>
    /// This class helps us with managing shoppingCart form buttons, labels etc.
    /// It has many methods that are called in the shoppingCart form, and it helps with keeping the products up to date with database.
    /// </summary>
    public class ShoppingCartClass
    {
        String connectionstring = "Data Source=SQL5097.site4now.net;Initial Catalog=db_a75582_oopproje;User Id=db_a75582_oopproje_admin;Password=7eQ2icRhm_au";
        SqlConnection connection;
        SqlDataAdapter adapter;
        SqlCommand command;

        Dictionary<string, string> paths = new Dictionary<string, string>();

        public List<ItemToPurchase> itemsToPurchase = new List<ItemToPurchase>();
        private string CustomerId;
        private int paymentAmount;
        private string paymentType;
        public int getPaymentAmount() { return this.paymentAmount; }
        public ShoppingCartClass(string cstmrID, Dictionary<string, string> dict)
        {
            paths = dict;
            paymentType = "cash";
            CustomerId = cstmrID;
            paymentAmount = 0;
            connection = new SqlConnection(connectionstring);
            connection.Open();

            string selectsentence = "Select * from ShoppingCartS";
            adapter = new SqlDataAdapter(selectsentence, connection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);

            List<string> keys = new List<string>(this.paths.Keys);
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                if(dataTable.Rows[i]["customerID"].ToString() == CustomerId)
                {
                    string path = "";
                    for(int j = 0; j < keys.Count(); j++)
                    {
                        if(dataTable.Rows[i]["productName"].ToString() == keys[j].ToString())
                        {
                            path = paths[keys[j]];
                        }
                    }
                    itemsToPurchase.Add(new ItemToPurchase(dataTable.Rows[i]["productName"].ToString(), Convert.ToUInt32(dataTable.Rows[i]["unitPrice"].ToString()), Convert.ToUInt32(dataTable.Rows[i]["quantity"].ToString()), path));
                    paymentAmount += Convert.ToInt32(dataTable.Rows[i]["totalPrice"].ToString());
                }
            }

            connection.Close();
        }
        /// seçilen itemi ekler
        public void addProduct(ItemToPurchase item) 
        {
            string name = item.name;
            int index = itemsToPurchase.FindIndex(x => x.name == (name));
            try
            {
                if (index < 0)
                {
                    itemsToPurchase.Add(item);
                    string sqlEkle = "Insert ShoppingCartS set quantity = @quantity,TotalPrice = @TotalPrice, where" + CustomerId + "= @customerId";
                    connection = new SqlConnection(connectionstring);
                    connection.Open();
                    command = new SqlCommand(sqlEkle, connection);
                    command.Parameters.AddWithValue("@customerID", int.Parse(CustomerId));
                    command.Parameters.AddWithValue("@productName", item.name);
                    command.Parameters.AddWithValue("@productType", item.GetType().ToString());
                    command.Parameters.AddWithValue("@quantity", item.amount);
                    command.Parameters.AddWithValue("@UnitPrice", item.price);
                    command.Parameters.AddWithValue("@TotalPrice", item.amount * item.price);
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                else
                    updateAmountOfProduct(index, item.amount + itemsToPurchase[index].amount);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

        }
        /// yalnızca seçilen itemi siler
        public void RemoveProduct(ItemToPurchase item) 
        {
            int index = itemsToPurchase.FindIndex(x => x.name == (item.name));
            itemsToPurchase.RemoveAt(index);
        }
        /// tüm sepeti tek tuşla boşaltır
        public void clearMyCart(int id) 
        {
            connection = new SqlConnection(connectionstring);
            connection.Open();

            string selectsentence = "Select * from ShoppingCartS";
            adapter = new SqlDataAdapter(selectsentence, connection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            try
            {
                string SelectDelete = "delete from ShoppingCartS where customerID=" + id.ToString();
                command = new SqlCommand(SelectDelete, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            connection.Close();
            itemsToPurchase.Clear();
        }
        /// ürünün sepetteki miktarını günceller
        public void updateAmountOfProduct(int index, uint newAmount) 
        {

            ItemToPurchase temp = itemsToPurchase[index];
            uint price = temp.price / temp.amount;
            temp.amount = newAmount;
            temp.price = newAmount * price;
            itemsToPurchase[index] = temp;

            string sqlEkle = "Update ShoppingCartS set"+ itemsToPurchase[index].amount +" = @quantity,"+ itemsToPurchase[index].price + "= @TotalPrice where" + CustomerId + "= @customerId AND" + itemsToPurchase[index].name + "= @productName";
            connection = new SqlConnection(connectionstring);
            connection.Open();
            command = new SqlCommand(sqlEkle, connection);
            command.Parameters.AddWithValue("@quantity", itemsToPurchase[index].amount);
            command.Parameters.AddWithValue("@TotalPrice", itemsToPurchase[index].price);
            command.ExecuteNonQuery();
            connection.Close();
        }

        public string[] GetListElement(int index)
        {
            string[] liste = { itemsToPurchase[index].name, itemsToPurchase[index].price.ToString(), itemsToPurchase[index].amount.ToString() };

            return liste;
        }
        public int GetListSize() { return itemsToPurchase.Count(); }

        public int GetIndex(string name)
        {
            int index = itemsToPurchase.FindIndex(x => x.name == (name));
            return index;
        }
        public void placeOrder()
        {
            sendInvoicebyEmail();
        }
        public void cancelOrder()
        {
            paymentAmount = 0;
            itemsToPurchase.Clear();
        }
        public void sendInvoicebyEmail()
        {
            MessageBox.Show("Email has been sent to your email address.\nYour order will be processed.");
        }
        public void sendInvoicebySMS()
        {
            MessageBox.Show("SMS has been sent to your phone number.\nYour order will be processed.");
        }
    }
}
