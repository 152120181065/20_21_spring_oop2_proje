﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.IO;
using System.Drawing.Imaging;

namespace OOP2_Project
{
    public partial class Shop : Form
    {   
        String connectionstring="Data Source=SQL5097.site4now.net;Initial Catalog=db_a75582_oopproje;User Id=db_a75582_oopproje_admin;Password=7eQ2icRhm_au";    
        SqlConnection connection;
        SqlDataAdapter adapter;
        List<Book> books = new List<Book>();
        List<Magazine> magazines = new List<Magazine>();
        List<CD> cds = new List<CD>();
        Customer loggedCustomer = new Customer();
        Profile profile = new Profile();
        Authorization aut = new Authorization();
        List<Customer> customers = new List<Customer>();

        private static Shop inst;

        public static Shop GetForm
        {
            get
            {
                if (inst == null || inst.IsDisposed)
                    inst = new Shop();
                return inst;

            }
        }

        public void setcustomers(List<Customer> aut_customers)
        {
            customers = aut_customers;
        }

        Dictionary<string, string> paths = new Dictionary<string, string>();
        
        public void setCustomer(Customer autCostumer)
        {
            loggedCustomer = autCostumer;
        }

        public Shop()
        {
            InitializeComponent();
            tabControl1.DrawItem += new DrawItemEventHandler(tabControl1_DrawItem);
            Shop_Load();
        }

        private void Shop_Load()
        {
            connection = new SqlConnection(connectionstring);
            try
            {
            connection.Open();

            listView1.View = View.LargeIcon;
            listView1.Columns.Add("Books", 150, HorizontalAlignment.Center);
            listView1.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.HeaderSize);

            listView2.View = View.LargeIcon;
            listView2.Columns.Add("CD", 150, HorizontalAlignment.Center);
            listView2.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.HeaderSize);

            listView3.View = View.LargeIcon;
            listView3.Columns.Add("Magazine", 150, HorizontalAlignment.Center);
            listView3.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.HeaderSize);

            FillBooks();
            FillCDs();
            FillMagazines();
            connection.Close();
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
                
            }      
        }
       private void FillBooks()
        {
            ImageList imgs = new ImageList();
            imgs.ColorDepth = ColorDepth.Depth32Bit;
            imgs.ImageSize = new Size(110, 121);    ////

            string selectsentence = "Select * from Books";
            adapter = new SqlDataAdapter(selectsentence,connection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            
            for(int i = 0; i < dataTable.Rows.Count; i++)
            {
                Book newbook = new Book(dataTable.Rows[i]["Name"].ToString(), uint.Parse(dataTable.Rows[i]["ID"].ToString()), uint.Parse(dataTable.Rows[i]["Price"].ToString()), long.Parse(dataTable.Rows[i]["isbn"].ToString()), dataTable.Rows[i]["Author"].ToString(), uint.Parse(dataTable.Rows[i]["Page"].ToString()));
                books.Add(newbook);
                paths.Add(dataTable.Rows[i]["Name"].ToString(), dataTable.Rows[i]["Path"].ToString());///////////
                imgs.Images.Add(Image.FromFile(Path.Combine(Environment.CurrentDirectory, dataTable.Rows[i]["Path"].ToString())));
            }

            listView1.LargeImageList = imgs;            
            for (int i = 0; i < books.Count(); i++)     
            {
                listView1.Items.Add(new ListViewItem(books[i].Name.ToString(), i));
            }          
        }
        private void FillMagazines()
        {
            ImageList imgs = new ImageList();
            imgs.ColorDepth = ColorDepth.Depth32Bit;
            imgs.ImageSize = new Size(110, 121);   

            string selectsentence = "Select * from Magazines";
            adapter = new SqlDataAdapter(selectsentence, connection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                MagazineType type = new MagazineType();
                if (dataTable.Rows[i]["Type"].ToString() == "Sport")
                    type = MagazineType.Sport;
                else if (dataTable.Rows[i]["Type"].ToString() == "Science")
                    type = MagazineType.Science;
                else if (dataTable.Rows[i]["Type"].ToString() == "Fashion")
                    type = MagazineType.Fashion;
                else if (dataTable.Rows[i]["Type"].ToString() == "Literature")
                    type = MagazineType.Literature;
                else if (dataTable.Rows[i]["Type"].ToString() == "Auto")
                    type = MagazineType.Auto;

                Magazine newmagazine = new Magazine(dataTable.Rows[i]["Name"].ToString(), uint.Parse(dataTable.Rows[i]["ID"].ToString()), uint.Parse(dataTable.Rows[i]["Price"].ToString()), uint.Parse(dataTable.Rows[i]["Issue"].ToString()), type);
                magazines.Add(newmagazine);
                paths.Add(dataTable.Rows[i]["Name"].ToString(), dataTable.Rows[i]["Path"].ToString());//////////
                imgs.Images.Add(Image.FromFile(Path.Combine(Environment.CurrentDirectory, dataTable.Rows[i]["Path"].ToString())));
            }

            listView2.LargeImageList = imgs;            ////
            for (int i = 0; i < books.Count(); i++)     ////
            {
                listView2.Items.Add(new ListViewItem(magazines[i].Name.ToString(), i));
            }
        }        
        private void FillCDs()
        {
            ImageList imgs = new ImageList();
            imgs.ColorDepth = ColorDepth.Depth32Bit;
            imgs.ImageSize = new Size(110, 121);    ////

            string selectsentence = "Select * from CD";
            adapter = new SqlDataAdapter(selectsentence, connection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                MusicType type = new MusicType();
                if (dataTable.Rows[i]["Type"].ToString() == "Rock")
                    type = MusicType.Rock;
                else if (dataTable.Rows[i]["Type"].ToString() == "Pop")
                    type = MusicType.Pop;
                else if (dataTable.Rows[i]["Type"].ToString() == "Country")
                    type = MusicType.Country;

                CD newcd = new CD(dataTable.Rows[i]["Name"].ToString(), uint.Parse(dataTable.Rows[i]["ID"].ToString()), uint.Parse(dataTable.Rows[i]["Price"].ToString()), dataTable.Rows[i]["Singer"].ToString(), type);
                cds.Add(newcd);
                paths.Add(dataTable.Rows[i]["Name"].ToString(), dataTable.Rows[i]["Path"].ToString());//////////
                imgs.Images.Add(Image.FromFile(Path.Combine(Environment.CurrentDirectory, dataTable.Rows[i]["Path"].ToString())));
            }

            listView3.LargeImageList = imgs;            ////
            for (int i = 0; i < books.Count(); i++)     ////
            {
                listView3.Items.Add(new ListViewItem(cds[i].Name.ToString(), i));
            }

        }
        private void tabControl1_DrawItem(Object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush _textBrush;

            // Get the item from the collection.
            TabPage _tabPage = tabControl1.TabPages[e.Index];

            // Get the real bounds for the tab rectangle.
            Rectangle _tabBounds = tabControl1.GetTabRect(e.Index);
            if (e.State == DrawItemState.Selected)
            {

                // Draw a different background color, and don't paint a focus rectangle.
                _textBrush = new SolidBrush(Color.Red);
                g.FillRectangle(Brushes.Gray, e.Bounds);
            }
            else
            {
                _textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                e.DrawBackground();
            }

            // Use our own font.
            Font _tabFont = new Font("Arial", 14.0f, FontStyle.Bold, GraphicsUnit.Pixel);

            // Draw string. Center the text.
            StringFormat _stringFlags = new StringFormat();
            _stringFlags.Alignment = StringAlignment.Center;
            _stringFlags.LineAlignment = StringAlignment.Center;
            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));
        }

        public bool isProfileClicked { get; set; }
        private void btnProfile_Click(object sender, EventArgs e)
        {
            isProfileClicked = true;
            profile.setCustomer(loggedCustomer);
            profile.setcustomers(customers);
            this.Hide();
            profile.Show();
        }

        private void btnSignout_Click(object sender, EventArgs e)
        {
            this.Close();         
        }

        private void btnCart_Click(object sender, EventArgs e)
        {
            ShoppingCart cart = new ShoppingCart();
            cart.customer = loggedCustomer;
            cart.paths = this.paths;
            cart.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Shop_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
                aut.Show();

        }

        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            ProductDescription proDesc = new ProductDescription();
            string prodName = listView1.FocusedItem.SubItems[0].Text.ToString();
            string path = "";
            string pr_isbn = "";
            string pr_price = "";
            string pr_page = "";
            string creator = "";
            string selectsentence = "Select * from Books";
            adapter = new SqlDataAdapter(selectsentence, connection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);


            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                if (prodName == dataTable.Rows[i]["Name"].ToString())
                {
                    creator = dataTable.Rows[i]["Author"].ToString();
                    pr_isbn = dataTable.Rows[i]["isbn"].ToString();
                    pr_page = dataTable.Rows[i]["Page"].ToString();
                    pr_price = dataTable.Rows[i]["Price"].ToString();
                    path = dataTable.Rows[i]["Path"].ToString();
                    break;
                }
            }

            proDesc.customer = loggedCustomer;
            proDesc.setType("Books");
            proDesc.setName(prodName);
            proDesc.setCreator(creator);
            proDesc.setISBN(pr_isbn);
            proDesc.setPageCount(pr_page);
            proDesc.setPrice(pr_price);
            proDesc.setPath(path);
            proDesc.Show();
        }

        private void listView2_MouseClick(object sender, MouseEventArgs e)
        {
            ProductDescription proDesc = new ProductDescription();
            string prodName = listView2.FocusedItem.SubItems[0].Text.ToString();
            string path = "";
            string pr_issue = "";
            string pr_price = "";
            string pr_type = "";
            string selectsentence = "Select * from Magazines";
            adapter = new SqlDataAdapter(selectsentence, connection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);


            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                if (prodName == dataTable.Rows[i]["Name"].ToString())
                {
                    pr_issue = dataTable.Rows[i]["Issue"].ToString();
                    pr_type = dataTable.Rows[i]["Type"].ToString();
                    pr_price = dataTable.Rows[i]["Price"].ToString();
                    path = dataTable.Rows[i]["Path"].ToString();
                    break;
                }
            }

            proDesc.customer = loggedCustomer;
            proDesc.setName(prodName);
            proDesc.setIssue(pr_issue);
            proDesc.setType(pr_type);
            proDesc.setPrice(pr_price);
            proDesc.setPath(path);
            proDesc.Show();
        }
        private void listView3_MouseClick(object sender, MouseEventArgs e)
        {
            ProductDescription proDesc = new ProductDescription();
            string prodName = listView3.FocusedItem.SubItems[0].Text.ToString();
            string path = "";
            string pr_price = "";
            string pr_type = "";
            string creator = "";
            string selectsentence = "Select * from CD";
            adapter = new SqlDataAdapter(selectsentence, connection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);


            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                if (prodName == dataTable.Rows[i]["Name"].ToString())
                {
                    creator = dataTable.Rows[i]["Singer"].ToString();
                    pr_type = dataTable.Rows[i]["Type"].ToString();
                    pr_price = dataTable.Rows[i]["Price"].ToString();
                    path = dataTable.Rows[i]["Path"].ToString();
                    break;
                }
            }

            proDesc.customer = loggedCustomer;
            proDesc.setName(prodName);
            proDesc.setCreator(creator);
            proDesc.setType(pr_type);
            proDesc.setPrice(pr_price);
            proDesc.setPath(path);
            proDesc.Show();
        }
    }
}
