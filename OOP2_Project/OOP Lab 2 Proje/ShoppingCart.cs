﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace OOP2_Project
{
    public partial class ShoppingCart : Form
    {
        bool isSelectedNull = true;
        public Customer customer = new Customer();
        public Dictionary<string, string> paths = new Dictionary<string, string>();
        ShoppingCartClass shoppingCart;

        String connectionstring = "Data Source=SQL5097.site4now.net;Initial Catalog=db_a75582_oopproje;User Id=db_a75582_oopproje_admin;Password=7eQ2icRhm_au";
        SqlConnection sqlConnection;
        SqlDataAdapter sqlAdapter;
        SqlCommand sqlCommand;

        public ShoppingCart()
        {
            InitializeComponent();     
        }

        private void ShoppingCart_Load(object sender, EventArgs e)
        {
            populate();
        }

        public void populate()
        {
            lv_Cart.View = View.Details;
            lv_Cart.Columns.Add("Product", 100);
            lv_Cart.Columns.Add("Amount", 50);
            lv_Cart.Columns.Add("Total Price", 50);

            shoppingCart = new ShoppingCartClass(customer.CustomerID.ToString(), paths);
            ImageList imgs = new ImageList();
            imgs.ImageSize = new Size(32, 32);

            for (int i = 0; i < shoppingCart.itemsToPurchase.Count(); i++)
            {
                imgs.Images.Add(Image.FromFile(Path.Combine(Environment.CurrentDirectory, shoppingCart.itemsToPurchase[i].path)));
            }

            lv_Cart.SmallImageList = imgs;

            for (int i = 0; i < shoppingCart.itemsToPurchase.Count(); i++)
            {
                ListViewItem lst = new ListViewItem(shoppingCart.itemsToPurchase[i].name, i);

                int total = Convert.ToInt32(shoppingCart.itemsToPurchase[i].price.ToString()) * Convert.ToInt32(shoppingCart.itemsToPurchase[i].amount.ToString());

                lst.SubItems.Add(shoppingCart.itemsToPurchase[i].amount.ToString());
                lst.SubItems.Add(total.ToString());
                lv_Cart.Items.Add(lst);
            }
        }

        private void lv_Cart_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv_Cart.FocusedItem == null)
            {
                isSelectedNull = true;
            }
            else
            {
                isSelectedNull = false;
                txtAmount.Text = lv_Cart.FocusedItem.SubItems[1].Text;
                lblError.Text = "";
            }
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            if (!isSelectedNull)
            {
                int temp = Int32.Parse(txtAmount.Text);
                temp++;
                txtAmount.Text = temp.ToString();
            }
        }

        private void btnMinus_Click(object sender, EventArgs e)
        {
            if (!isSelectedNull)
            {
                int temp = Int32.Parse(txtAmount.Text);
                if (temp > 1)
                    temp--;
                else
                    lblError.Text = "Amount can't be\n less than 1.";
                txtAmount.Text = temp.ToString();
            }
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                sqlConnection = new SqlConnection(connectionstring);
                sqlConnection.Open();
                string selectID = "Select * from ShoppingCartS";
                sqlAdapter = new SqlDataAdapter(selectID, sqlConnection);
                DataTable dataTable = new DataTable();
                sqlAdapter.Fill(dataTable);
                string dbname = "";
                string appname = "";
                int dbid = 0;
                int appid = 0;
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    dbname = dataTable.Rows[i]["productName"].ToString();
                    appname = lv_Cart.FocusedItem.SubItems[0].Text.ToString();
                    dbid = Convert.ToInt32(dataTable.Rows[i]["customerID"]);
                    appid = customer.getID();
                    if (appid == dbid && appname == dbname)
                    {
                        string updateS = "Update ShoppingCartS set quantity = @quantity, totalPrice = @totalPrice where customerID ='" + customer.getID() + "' AND productName ='" + lv_Cart.FocusedItem.SubItems[0].Text.ToString() + "'";

                        SqlCommand update = new SqlCommand(updateS, sqlConnection);
                        int quanty = Convert.ToInt32(txtAmount.Text.ToString());
                        int totalamount = Convert.ToInt32(dataTable.Rows[i]["unitPrice"].ToString()) * quanty;
                        update.Parameters.AddWithValue("@quantity", quanty);
                        update.Parameters.AddWithValue("@totalPrice", totalamount);
                        update.ExecuteNonQuery();
                    }
                }
                sqlConnection.Close();
                lv_Cart.Clear();
                populate();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (!isSelectedNull && lv_Cart.Items.Count > 0)
                {
                    sqlConnection = new SqlConnection(connectionstring);
                    sqlConnection.Open();
                    int id = 0;
                    id = customer.getID();
                    string name = lv_Cart.FocusedItem.SubItems[0].Text;
                    string selectsentence = "Select * from ShoppingCartS";
                    sqlAdapter = new SqlDataAdapter(selectsentence, sqlConnection);
                    DataTable dataTable = new DataTable();
                    sqlAdapter.Fill(dataTable);
                    try
                    {
                        string SelectDelete = "delete from ShoppingCartS where customerID=" + id.ToString() + "and productName='" + name + "'";
                        sqlCommand = new SqlCommand(SelectDelete, sqlConnection);
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show(Ex.Message);
                    }
                    sqlConnection.Close();
                    lv_Cart.FocusedItem.Remove();
                    txtAmount.Text = "";
                    isSelectedNull = true;
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }
        private void btnPurchase_Click(object sender, EventArgs e)
        {
            txtAmount.Text = "";
            int cust_id = customer.getID();
            

            // Captures the current time in a DateTime object.
            DateTime currentTime = DateTime.Now;

            sqlConnection = new SqlConnection("Data Source=SQL5097.site4now.net; Initial Catalog=db_a75582_oopproje; User ID=db_a75582_oopproje_admin; Password=7eQ2icRhm_au");
            sqlConnection.Open();

            string sorgu = "Insert Into Purchases(situation,purchaseDate,products,totalPrice,customerID) values(@situation,@purchaseDate,@products,@totalPrice,@customerID)";
            sqlCommand = new SqlCommand(sorgu, sqlConnection);
            sqlCommand.Parameters.AddWithValue("@situation", "PROCESSINNG");
            sqlCommand.Parameters.AddWithValue("@purchaseDate", currentTime);
            sqlCommand.Parameters.AddWithValue("@products", shoppingCart.itemsToPurchase.Count);
            sqlCommand.Parameters.AddWithValue("@totalPrice", shoppingCart.getPaymentAmount());
            sqlCommand.Parameters.AddWithValue("@customerID", customer.getID());

            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
            txtAmount.Text = "";
            shoppingCart.clearMyCart(cust_id);
            lv_Cart.Items.Clear();
            shoppingCart.placeOrder();
        }

        private void btnGoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClearCart_Click_1(object sender, EventArgs e)
        {
            int cust_id = 0;
            cust_id = customer.getID();
            shoppingCart.clearMyCart(cust_id);
            lv_Cart.Items.Clear();
        }
    }
}
