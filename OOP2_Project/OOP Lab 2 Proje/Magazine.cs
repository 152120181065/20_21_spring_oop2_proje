﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_Project
{
    /*! \brief This class is a template for magazines.
   *  With this class and a list program can store magazines which customer bought/will buy.
   *  It is a descendant of abstract class Product.
   *  
   *  It has 5 values;
   *  @param name keeps name of the magazine.
   *  @param id keeps unique number of the magazine.
   *  @param price keeps price of the magazine.
   *  These three values are common with other inherited classes from product (CDs and Magazines).
   *  @param issue keeps the magazine's issue number.
   *  @param type keeps the magazine's type. It is an enum type.
   */
    class Magazine : Product
    {
        string name;
        uint id;
        uint price;
        uint issue;
        MagazineType type;
        
        public string Name { get { return name; } set { } }
        public uint ID { get { return id; } set { } }
        public uint Price { get { return price; } set { } }
        public uint Issue { get { return issue; } set { } }
        public MagazineType Type { get { return type; } set { } }
        
        public Magazine() { }
        public Magazine(string name, uint id, uint price, uint issue, MagazineType type)
        {
            this.name = name;
            this.id = id;
            this.price = price;
            this.issue = issue;
            this.type = type;
        }
        public override void printProperties()
        {
            Console.WriteLine("Name: " + name + Environment.NewLine + "ID: " + id + Environment.NewLine + "Price: " + price + Environment.NewLine);
            Console.WriteLine("Issue: " + issue + Environment.NewLine + "Type: " + type + Environment.NewLine);
        }
    }

    enum MagazineType
    {
        Sport,
        Auto,
        Science,
        Fashion,
        Literature
    }
}
