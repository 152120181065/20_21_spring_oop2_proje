﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_Project
{
    /*! \brief This class is a template for CDs.
    *  With this class and a list program can store cds which customer bought.
    *  It is a descendant of abstract class Product.
    *  
    *  It has 5 values;
    *  @param name keeps name of the CD.
    *  @param id keeps unique number of the CD.
    *  @param price keeps price of the CD
    *  These three values are common with other inherited classes from product.
    *  @param singer keeps singer's name of the CD.
    *  @param type keeps type of the music. It is an enum type.
    */
    class CD : Product
    {
        string name;
        uint id;
        uint price;
        string singer;
        MusicType type;
        
        public string Name { get { return name; } set {} }
        public uint ID { get { return id; } set {} }
        public uint Price { get { return price; } set {} }
        public string Singer { get { return singer; } set {} }
        public MusicType Type { get { return type; } set {} }

        public CD() { }
        public CD(string name,uint id,uint price, string singer,MusicType type) {
            this.name = name;
            this.id = id;
            this.price = price;
            this.singer = singer;
            this.type = type;
        }
        public override void printProperties()
        {
            Console.WriteLine("Name: " + name + Environment.NewLine + "ID: " + id + Environment.NewLine + "Price: " + price + Environment.NewLine);
            Console.WriteLine("Singer: " + singer + Environment.NewLine + "Type: " + type + Environment.NewLine);
        }
    }
    enum MusicType
    {
        Rock,
        Pop,
        Country
    }
}

