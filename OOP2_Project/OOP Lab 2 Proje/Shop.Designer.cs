﻿namespace OOP2_Project
{
    partial class Shop
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Shop));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.BookTab = new System.Windows.Forms.TabPage();
            this.listView1 = new System.Windows.Forms.ListView();
            this.MagazineTab = new System.Windows.Forms.TabPage();
            this.listView2 = new System.Windows.Forms.ListView();
            this.CdTab = new System.Windows.Forms.TabPage();
            this.listView3 = new System.Windows.Forms.ListView();
            this.lblState = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnProfile = new System.Windows.Forms.Button();
            this.btnCart = new System.Windows.Forms.Button();
            this.btnSignout = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.BookTab.SuspendLayout();
            this.MagazineTab.SuspendLayout();
            this.CdTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.Controls.Add(this.BookTab);
            this.tabControl1.Controls.Add(this.MagazineTab);
            this.tabControl1.Controls.Add(this.CdTab);
            this.tabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControl1.ItemSize = new System.Drawing.Size(103, 100);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(574, 420);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 1;
            // 
            // BookTab
            // 
            this.BookTab.BackColor = System.Drawing.Color.Linen;
            this.BookTab.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BookTab.Controls.Add(this.listView1);
            this.BookTab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BookTab.Location = new System.Drawing.Point(104, 4);
            this.BookTab.Name = "BookTab";
            this.BookTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.BookTab.Size = new System.Drawing.Size(466, 412);
            this.BookTab.TabIndex = 0;
            this.BookTab.Text = "Books";
            // 
            // listView1
            // 
            this.listView1.BackColor = System.Drawing.Color.Linen;
            this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(2, 6);
            this.listView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(462, 398);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseClick);
            // 
            // MagazineTab
            // 
            this.MagazineTab.BackColor = System.Drawing.Color.Linen;
            this.MagazineTab.Controls.Add(this.listView2);
            this.MagazineTab.Location = new System.Drawing.Point(104, 4);
            this.MagazineTab.Name = "MagazineTab";
            this.MagazineTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.MagazineTab.Size = new System.Drawing.Size(466, 412);
            this.MagazineTab.TabIndex = 1;
            this.MagazineTab.Text = "Magazine";
            // 
            // listView2
            // 
            this.listView2.BackColor = System.Drawing.Color.Linen;
            this.listView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listView2.HideSelection = false;
            this.listView2.Location = new System.Drawing.Point(2, 6);
            this.listView2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(459, 398);
            this.listView2.TabIndex = 0;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listView2_MouseClick);
            // 
            // CdTab
            // 
            this.CdTab.BackColor = System.Drawing.Color.Linen;
            this.CdTab.Controls.Add(this.listView3);
            this.CdTab.Location = new System.Drawing.Point(104, 4);
            this.CdTab.Name = "CdTab";
            this.CdTab.Size = new System.Drawing.Size(466, 412);
            this.CdTab.TabIndex = 2;
            this.CdTab.Text = "CD";
            // 
            // listView3
            // 
            this.listView3.BackColor = System.Drawing.Color.Linen;
            this.listView3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listView3.HideSelection = false;
            this.listView3.Location = new System.Drawing.Point(0, 9);
            this.listView3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(462, 398);
            this.listView3.TabIndex = 0;
            this.listView3.UseCompatibleStateImageBehavior = false;
            this.listView3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listView3_MouseClick);
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(580, 109);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(0, 13);
            this.lblState.TabIndex = 7;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Brown;
            this.btnExit.BackgroundImage = global::OOP2_Project.Properties.Resources.exit;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnExit.Location = new System.Drawing.Point(577, 330);
            this.btnExit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(95, 90);
            this.btnExit.TabIndex = 9;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnProfile
            // 
            this.btnProfile.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnProfile.BackgroundImage")));
            this.btnProfile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnProfile.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnProfile.ForeColor = System.Drawing.Color.Sienna;
            this.btnProfile.Location = new System.Drawing.Point(0, 313);
            this.btnProfile.Name = "btnProfile";
            this.btnProfile.Size = new System.Drawing.Size(103, 106);
            this.btnProfile.TabIndex = 8;
            this.btnProfile.Text = "My Account";
            this.btnProfile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnProfile.UseVisualStyleBackColor = true;
            this.btnProfile.Click += new System.EventHandler(this.btnProfile_Click);
            // 
            // btnCart
            // 
            this.btnCart.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCart.BackgroundImage")));
            this.btnCart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnCart.ForeColor = System.Drawing.Color.Sienna;
            this.btnCart.Location = new System.Drawing.Point(576, 4);
            this.btnCart.Name = "btnCart";
            this.btnCart.Size = new System.Drawing.Size(95, 91);
            this.btnCart.TabIndex = 5;
            this.btnCart.Text = "Cart";
            this.btnCart.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCart.UseVisualStyleBackColor = true;
            this.btnCart.Click += new System.EventHandler(this.btnCart_Click);
            // 
            // btnSignout
            // 
            this.btnSignout.BackColor = System.Drawing.Color.White;
            this.btnSignout.BackgroundImage = global::OOP2_Project.Properties.Resources.signoutv2;
            this.btnSignout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSignout.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSignout.ForeColor = System.Drawing.Color.Sienna;
            this.btnSignout.Location = new System.Drawing.Point(577, 228);
            this.btnSignout.Name = "btnSignout";
            this.btnSignout.Size = new System.Drawing.Size(95, 90);
            this.btnSignout.TabIndex = 4;
            this.btnSignout.Text = "Sign Out";
            this.btnSignout.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSignout.UseVisualStyleBackColor = false;
            this.btnSignout.Click += new System.EventHandler(this.btnSignout_Click);
            // 
            // Shop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Linen;
            this.ClientSize = new System.Drawing.Size(671, 419);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnProfile);
            this.Controls.Add(this.lblState);
            this.Controls.Add(this.btnCart);
            this.Controls.Add(this.btnSignout);
            this.Controls.Add(this.tabControl1);
            this.Name = "Shop";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Shop_FormClosed);
            this.tabControl1.ResumeLayout(false);
            this.BookTab.ResumeLayout(false);
            this.MagazineTab.ResumeLayout(false);
            this.CdTab.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage BookTab;
        private System.Windows.Forms.TabPage MagazineTab;
        private System.Windows.Forms.TabPage CdTab;
        private System.Windows.Forms.Button btnSignout;
        private System.Windows.Forms.Button btnCart;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ListView listView3;
        private System.Windows.Forms.Button btnProfile;
    }
}

