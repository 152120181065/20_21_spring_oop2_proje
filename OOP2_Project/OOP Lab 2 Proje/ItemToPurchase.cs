﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_Project
{
    /*! \brief This struct helps us with the items that will be purchased.
    *  It keeps the data for the products.
    * @param name keeps the items's name.
    * @param price keeps the items's price.
    * @param amount keeps the items's quantity.
    * @param path keeps the items's path.
    */
    public struct ItemToPurchase
    {
        public ItemToPurchase(string name, uint price, uint amount, string path)
        {
            this.name = name;
            this.amount = amount;
            this.price = price;
            this.path = path;
        }
        public string name;
        public uint price;
        public uint amount;
        public string path;
    }
}
